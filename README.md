# qmesh-qgis-plugins #

Welcome the the qmesh-qgis-plugins code development repository.

### What is qmesh-qgis-plugins? ###

qmesh-qgis-plugins is the Graphical User Interface of [qmesh](http://qmesh.org). The GUI is in the form of a [QGIS plugin](https://plugins.qgis.org/).
Only host the plugin code is hosted here, the qmesh core is hosted in a [separate repository](https://bitbucket.org/qmesh-developers/qmesh).

### Configuration & Installation

Please see the relevant [wiki page](https://bitbucket.org/qmesh-developers/qmesh-qgis-plugins/wiki/Installation).

## Contributors

The lead developers of qmesh are listed in file [AUTHORS.md](./AUTHORS.md)

## License

QMesh is available under the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html). Please see the file [LICENSE](./LICENSE) for more information.
