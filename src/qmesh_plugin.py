# -*- coding: utf-8 -*-
#    Copyright (C) 2014 Alexandros Avdis and others. See the AUTHORS file for a full list of copyright holders.
#
#    This file is part of qmesh_plugin.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    qmesh_plugin is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with qmesh_plugin.  If not, see <http://www.gnu.org/licenses/>.
"""
/***************************************************************************
 qmesh_plugin
                                 A QGIS plugin
                              FE meshes from GIS
                              -------------------
        begin                : 2014-08-21
        copyright            : (C) 2014 Alexandros Avdis, Jon Hill
        email                : support@qmesh.org
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   qmesh_plugin is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with QMesh.  If not, see <http://www.gnu.org/licenses/>.        *
 *                                                                         *
 *                                                                         *
 ***************************************************************************/
"""
# Import the PyQt and QGIS libraries
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from qgis.core import *
# Initialize Qt resources from file resources.py
import resources_rc
# Import the code for the dialog
from qmeshdialog import qmeshDialog, qmeshMeshMetricDialog, qmeshAboutDialog
import os.path
from qgis.gui import *
import qmesh
import sys

class EmittingStream(QObject):

    textWritten = pyqtSignal(str)

    def write(self, text):
        self.textWritten.emit(str(text))

    def flush(self):
        pass


class qmesh_plugin:

    def __init__(self, iface):
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value("locale/userLocale")[0:2]
        localePath = os.path.join(self.plugin_dir, 'i18n', 'qmesh_plugin_{}.qm'.format(locale))

        if os.path.exists(localePath):
            self.translator = QTranslator()
            self.translator.load(localePath)

            if qVersion() > '4.3.3':
                QCoreApplication.installTranslator(self.translator)

        # Create the dialog (after translation) and keep reference
        self.dlg = qmeshDialog()
        # Connect up the cancel and generate mesh buttons
        self.dlg.qmeshDialog_generateMeshButton.clicked.connect(self.generateMesh)
        self.dlg.qmeshDialog_cancelButton.clicked.connect(self.dlg.close)
        # Connect up the browse button
        self.dlg.qmeshDialog_browseButton.clicked.connect(self.selectFile)
        
        # Mesh metric dialog
        self.dlg_meshmetric = qmeshMeshMetricDialog()
        self.output = ""
        
        # Install the custom output stream
        sys.stdout = EmittingStream(textWritten=self.normalOutputWritten)
    
    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__

    def initGui(self):
        """ Sets up the menu system and links clikcing on them to 
        functions. The functions do the hard work"""

        # Create action that will start plugin configuration

        self.aboutAction = QAction(QIcon(":/plugins/qmesh_plugin/qmesh.png"), "About", self.iface.mainWindow())
        QObject.connect(self.aboutAction, SIGNAL("triggered()"), self.about)

        self.meshMetricAction = QAction(QIcon(":/plugins/qmesh_plugin/qmesh.png"), "Mesh metric creation", self.iface.mainWindow())
        QObject.connect(self.meshMetricAction, SIGNAL("triggered()"), self.mesh_metric)


        # Add menu items
        self.menu = self.iface.pluginMenu().addMenu(QIcon(":/plugins/qmesh_plugin/qmesh.png"), "QMesh")
        self.menu.addAction(self.aboutAction)
        self.menu.addAction(self.meshMetricAction)

        self.action = QAction(
            QIcon(":/plugins/qmesh_plugin/icon.png"),
            u"QMesh", self.iface.mainWindow())

        # connect the action to the run method
        self.action.triggered.connect(self.runQmesh)

        # Add toolbar button and menu item
        self.iface.addToolBarIcon(self.action)
        self.menu.addAction(self.action)  

    def about(self):
        """Display an about dialog"""
        d = qmeshAboutDialog()
        d.show()
        res = d.exec_()

    def mesh_metric(self):
        d = self.dlg_meshmetric

        # populate dropdown lists
        layers = self.iface.legendInterface().layers()
        d.meshmetricDialog_distInputVectorCombo.clear()
        d.meshmetricDialog_waveInputBathCombo.clear()
        for layer in layers:
            layerType = layer.type()
            name  = layer.name()            
            if layerType == QgsMapLayer.VectorLayer:
                # add to vector list
                d.meshmetricDialog_distInputVectorCombo.addItem(name,layer)
            elif layerType == QgsMapLayer.RasterLayer:
                # add to raster list
                d.meshmetricDialog_waveInputBathCombo.addItem(name, layer)

        # connect up the cancel button
        d.meshmetric_closeButton.clicked.connect(d.close)
        
        # connect up the browse buttons
        d.meshmetricDialog_distOutputRasterBrowseButton.clicked.connect(self.meshmetric_distBrowseButtonClicked)
        d.meshmetricDialog_waveOutputRasterBrowseButton.clicked.connect(self.meshmetric_waveBrowseButtonClicked)

        # connect up generate buttons
        d.meshmetricDialog_distGenerateButton.clicked.connect(self.meshmetricDialog_distGenerateButton)
        d.meshmetricDialog_waveGenerateButton.clicked.connect(self.meshmetricDialog_waveGenerateButton)

        d.show()
        res = d.exec_()

    def meshmetricDialog_waveGenerateButton(self):
        # get the metric distance params
        pass

    def meshmetricDialog_distGenerateButton(self):
        pass

    def meshmetric_distBrowseButtonClicked(self):
        self.dlg_meshmetric.meshmetricDialog_distOutputRasterFileLine.setText(QFileDialog.getSaveFileName(None, caption="Select File"))

    def meshmetric_waveBrowseButtonClicked(self):
        self.dlg_meshmetric.meshmetricDialog_waveOutputRasterFileLine.setText(QFileDialog.getSaveFileName(None, caption="Select File"))

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.removePluginMenu(u"&QMesh", self.action)
        self.iface.removeToolBarIcon(self.action)

    def selectFile(self):
        self.output = QFileDialog.getSaveFileName(None, caption="Select File")
        if(self.output == ""):
           # If no file is specified, do not enable the "Generate Mesh" button yet.
           return
        else:
           # Handle the case where the .geo and/or .msh file exists.
           if(os.path.exists(self.output + ".geo") or os.path.exists(self.output + ".msh")):
              response = QMessageBox.question(None, 'Overwrite?', 'File %s and/or %s exists. Are you sure you want to overwrite this file?' % (self.output+".geo", self.output+".msh"), buttons = QMessageBox.Ok|QMessageBox.Cancel)
              if response == QMessageBox.Cancel:
                 return
           self.dlg.qmeshDialog_outputpath.setText(self.output)
           self.dlg.qmeshDialog_generateMeshButton.setEnabled(True)
           self.dlg.qmeshDialog_generateMeshButton.setText("Generate Mesh")

    def normalOutputWritten(self, text):
        """Append text to the QTextEdit."""
        # Maybe QTextEdit.append() works as well, but this is how I do it:
        cursor = self.dlg.qmeshDialog_progressText.textCursor()
        cursor.movePosition(QTextCursor.End)
        cursor.insertText(text)
        self.dlg.qmeshDialog_progressText.setTextCursor(cursor)
        self.dlg.qmeshDialog_progressText.ensureCursorVisible()


    # run method that performs all the real work
    def runQmesh(self):
        # show the dialog
        self.dlg.show()
               
        # populate dropdown lists
        layers = self.iface.legendInterface().layers()
        self.dlg.qmeshDialog_boundaryVectorCombo.clear()
        self.dlg.qmeshDialog_meshmetricCombo.clear()
        self.dlg.qmeshDialog_meshmetricCombo.addItem("",None)
        self.dlg.qmeshDialog_polygonCombo.clear()
        self.dlg.qmeshDialog_polygonCombo.addItem("",None)
        for layer in layers:
            layerType = layer.type()
            name  = layer.name()            
            if layerType == QgsMapLayer.VectorLayer:
                # add to vector list
                self.dlg.qmeshDialog_boundaryVectorCombo.addItem(name,layer)
                self.dlg.qmeshDialog_polygonCombo.addItem(name, layer) 
            elif layerType == QgsMapLayer.RasterLayer:
                # add to raster list
                self.dlg.qmeshDialog_meshmetricCombo.addItem(name, layer)
                
        # the Target CRS
        self.dlg.qmeshDialog_tcrsCombo.clear()
        # pull current CRS
        canvas = self.iface.mapCanvas()
        mapRenderer = canvas.mapRenderer()
        srs=mapRenderer.destinationCrs()
        self.dlg.qmeshDialog_tcrsCombo.addItem("Use current CRS",srs)
        self.dlg.qmeshDialog_tcrsCombo.addItem("PCC","PCC")

        
    def generateMesh(self):
        import subprocess
        plugin_pid = subprocess.os.getpid()
        qmesh_log_filename = '/tmp/qmesh_log_'+str(plugin_pid)
        qmesh.setLogOutputFile(qmesh_log_filename)
        qmesh.initialise()
        # grab the chosen raster and vector layer - note raster and polygon may be empty.
        index = self.dlg.qmeshDialog_boundaryVectorCombo.currentIndex()
        vector_layer = self.dlg.qmeshDialog_boundaryVectorCombo.itemData(index)
        index = self.dlg.qmeshDialog_meshmetricCombo.currentIndex()
        raster_layer = self.dlg.qmeshDialog_meshmetricCombo.itemData(index)
        index = self.dlg.qmeshDialog_polygonCombo.currentIndex()
        polygon_layer = self.dlg.qmeshDialog_polygonCombo.itemData(index)

        # get tcs
        index = self.dlg.qmeshDialog_tcrsCombo.currentIndex()
        tcs = self.dlg.qmeshDialog_tcrsCombo.itemData(index)

        # default params
        isGlobal = False
        default_PhysID = 666
        smallestMeshArea = 0
        surfacePhysID = 100

        #Load boundaries, and if given polygons. If polygons are
        # not given, detect loops and cnstruct polygons. Also
        # set the geometry of the domain
        boundaries = qmesh.vector.Shapes()
        domain = qmesh.mesh.Domain()
        boundaries.fromFile(str(vector_layer.source()))
        # does the user have a polygon layer set?
        if polygon_layer == None:
            loopShapes = qmesh.vector.identifyLoops(boundaries,
                    isGlobal=isGlobal, defaultPhysID=default_PhysID,
                    fixOpenLoops=True)
            polygonShapes = qmesh.vector.identifyPolygons(loopShapes,
                isGlobal=isGlobal, smallestMeshedArea=smallestMeshArea, 
                meshedAreaPhysID = surfacePhysID)
            domain.setGeometry(loopShapes, polygonShapes)
        else:
            polygonShapes = qmesh.vector.shapefileTools.Shapes()
            polygonShapes.fromFile(str(polygon_layer.source()))
            domain.setGeometry(boundaries, polygonShapes)
        # Set output coordinate reference system
        domain.setTargetCoordRefSystem(str(tcs))
        #Set the mesh metric field, if given
        if raster_layer != None:
            meshMetricRaster = qmesh.raster.raster()
            meshMetricRaster.fromFile(str(raster_layer.source()))
            domain.setMeshMetricField(meshMetricRaster)
            fldFilename = self.output+'.fld'
        else:
            fldFilename = None
        #Get output directory and construct filenames
        mshFilename= self.output+'.msh'
        geoFilename = self.output+'.geo'
        #Mesh the domain.
        domain.gmsh(geoFilename, \
                    fldFilename, \
                    mshFilename)
        #Convert mesh into shapefiles and display
        shpFilename = self.output+'_'+str(tcs)+'.shp'
        mesh = qmesh.mesh.Mesh()
        mesh.readGmsh(mshFilename, str(tcs))
        mesh.writeShapefile(shpFilename)
        vlayer = self.iface.addVectorLayer(shpFilename, "mesh", "ogr")
            
